#!/bin/bash

set -eu -o pipefail

CONFIG_FILE="/app/data/commento.conf"

echo "=> Ensure directores"
mkdir -p /app/data/
chown cloudron:cloudron -R /app/data/

echo "=> Ensure custom config file"
if [[ ! -f "${CONFIG_FILE}" ]]; then
    echo -e "#COMMENTO_GOOGLE_KEY=\n#COMMENTO_GOOGLE_SECRET=\n#COMMENTO_FORBID_NEW_OWNERS=false" > "${CONFIG_FILE}"
fi

# The exported env vars take precedence over the config file
echo "=> Prepare environment"
export COMMENTO_ORIGIN="${CLOUDRON_APP_ORIGIN}"
export COMMENTO_BIND_ADDRESS="0.0.0.0"
export COMMENTO_PORT=8080
export COMMENTO_POSTGRES="${CLOUDRON_POSTGRESQL_URL}?sslmode=disable"
export COMMENTO_SMTP_USERNAME="${CLOUDRON_MAIL_SMTP_USERNAME}"
export COMMENTO_SMTP_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export COMMENTO_SMTP_HOST="${CLOUDRON_MAIL_SMTP_SERVER}"
export COMMENTO_SMTP_PORT=${CLOUDRON_MAIL_STARTTLS_PORT}
export COMMENTO_SMTP_FROM_ADDRESS="${CLOUDRON_MAIL_FROM}"
export COMMENTO_SMTP_SKIP_HOST_VERIFY="true"
export COMMENTO_CONFIG_FILE="${CONFIG_FILE}"

echo "=> Start application"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/commento
