FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code/ /app/pkg/
WORKDIR /app/code

ARG VERSION=1.8.7

RUN cd /app/code && curl -L https://github.com/souramoo/commentoplusplus/releases/download/v${VERSION}/release.tar.gz | tar zxvf - --strip-components 1

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
