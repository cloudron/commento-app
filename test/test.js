#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    var browser;
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var email = process.env.EMAIL;

    const DOMAIN_NAME = 'Test Domain';
    const DOMAIN_URI = 'http://localhost:3030';

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function signup() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/signup`);

        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('name')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.id('password2')).sendKeys(password);
        await browser.findElement(By.id('signup-button')).click();

        await waitForElement(By.xpath('//div[contains(text(), "Confirmation Email Sent!")]'));
    }

    async function confirmSignup() {
        execSync(`cloudron push --app ${app.id} getConfirmHex.sh /tmp/`);

        var confirmHex = execSync(`cloudron exec --app ${app.id} -- /bin/bash /tmp/getConfirmHex.sh`).toString().split('\n')[1];
        var signupConfirmLink = `https://${app.fqdn}/api/owner/confirm-hex?confirmHex=${confirmHex}`;

        await browser.manage().deleteAllCookies();
        await browser.get(signupConfirmLink);

        await waitForElement(By.xpath('//div[contains(text(), "Successfully confirmed! Login to continue.")]'));
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text() = "Login"]')).click();

        await browser.wait(until.elementLocated(By.id('domain-add')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);

        await waitForElement(By.id('email'));
    }

    async function addDomain() {
        await browser.get(`https://${app.fqdn}/dashboard#new-domain-modal`);

        await browser.findElement(By.id('new-domain-name')).sendKeys(DOMAIN_NAME);
        await waitForElement(By.id('new-domain-domain'));
        await browser.findElement(By.id('new-domain-domain')).sendKeys(DOMAIN_URI);
        await browser.findElement(By.id('add-site-button')).click();

        await waitForElement(By.xpath(`//div[contains(text(),"${DOMAIN_NAME}")]`));
    }

    async function getDomain() {
        await browser.get(`https://${app.fqdn}/dashboard`);

        await waitForElement(By.xpath(`//div[contains(text(),"${DOMAIN_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can signup', signup);
    it('can confirm signup', confirmSignup);
    it('can login', login);
    it('can add domain', addDomain);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id io.commento.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can signup', signup);
    it('can confirm signup', confirmSignup);
    it('can login', login);
    it('can add domain', addDomain);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can login', login);
    it('can get domain', getDomain);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
