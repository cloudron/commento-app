Commento is a fast, privacy-focused commenting platform.

This app packages [Commento++](https://github.com/souramoo/commentoplusplus) a maintained fork of the original Commento.

### Features

* **Sticky Comments**: Pin important comments to the top of the thread.
* **Upvotes and Downvotes**: Upvote insightful comments and downvote ones that don't add to the discussion.
* **Markdown Support**: Allows for rich formatting and expression.
* **Nested Threads**: With nested comments, it is easy and intuitive to follow the discussion.
* **Automated Spam Detection**: With built-in spam detection, you and your readers have nothing to worry about.
* **Moderation Tools**: Remove offtopic comments, shadow ban spammers and trolls, and enforce timeouts.
